<?php


namespace Boitebeet\NovaChangesLog;


use Laravel\Nova\Card;

class ChangeLog extends Card
{
    public $width = 'full';
    public $component = 'nova-changes-log';
}