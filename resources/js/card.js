import ChangesLog from "./components/ChangesLog";

Nova.booting((Vue, router, store) => {
  Vue.component('nova-changes-log', ChangesLog);
})
