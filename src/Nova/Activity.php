<?php

namespace Boitebeet\NovaChangesLog\Nova;

use Boitebeet\NovaCompareField\Compare;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Resource;
use Spatie\Activitylog\ActivitylogServiceProvider;

class Activity extends Resource
{
    public static $displayInNavigation = false;
    public static $globallySearchable = false;
    public static $searchable = false;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \Spatie\Activitylog\Models\Activity::class;

    public function __construct($resource)
    {
        static::$model = ActivitylogServiceProvider::determineActivityModel();
        parent::__construct($resource);
    }

    public function title()
    {
        return __('Change') . ': ' . $this->id;
    }

    public static function label()
    {
        return __('Changes');
    }

    public static function singularLabel()
    {
        return __('Change');
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            MorphTo::make(__('Subject'), 'subject'),

            Text::make(__('Action'), function (){
                return __(Str::title($this->resource->description));
            }),

            MorphTo::make(__('Changed By'), 'causer'),

            Compare::make(__('Diff'), 'properties.attributes', 'properties.old'),

            DateTime::make(__('Changed On'), 'updated_at')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
