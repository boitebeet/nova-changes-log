<?php

namespace Boitebeet\NovaChangesLog;

use Boitebeet\NovaChangesLog\Nova\Activity;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class ChangesLogCardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadAndPublishTranslations();

        Nova::serving(function (ServingNova $event) {
            if (config('activitylog.shouldRegisterResource', true)) {
                Nova::resources([Activity::class]);
            }
            Nova::script('nova-changs-log', __DIR__.'/../dist/js/card.js');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function loadAndPublishTranslations()
    {
        if(is_dir(resource_path('lang/vendor/nova-changes-log'))){
            $this->loadJsonTranslationsFrom(resource_path('lang/vendor/nova-changes-log'));
        } else {
            $this->loadJsonTranslationsFrom(__DIR__.'/../resources/lang');
        }

        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/nova-changes-log'),
        ], 'nova-changes-log-lang');
    }
}
